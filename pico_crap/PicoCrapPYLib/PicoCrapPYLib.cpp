#include "PicoCrapPYLib.h"

using namespace std;

pico_serial ps;

static PyObject *writeSerial(PyObject *self, PyObject *args) {
	unsigned char *buffer;
	int dataLength = 0;
	if (!PyArg_ParseTuple(args, "si", &buffer, &dataLength)) {
		return NULL;
	}

	if (!ps.isOpen()) {
		return NULL;
	}
	int written = ps.writeCmd(buffer, dataLength);
	return Py_BuildValue("i", written);
}

static PyObject *readSerial(PyObject *self, PyObject *args) {
	int length = 0;
	unsigned char *ptr;
	if (!PyArg_ParseTuple(args, "si", &length, &ptr)) {
		return NULL;
	}
	int read = ps.read(ptr, length);
	return Py_BuildValue("i", read);
}

static PyObject *openSerial(PyObject *self, PyObject *args) {
	TCHAR *comList[] = { TEXT("\\\\.\\COM1"), TEXT("\\\\.\\COM2"), TEXT("\\\\.\\COM3"), TEXT("\\\\.\\COM4"), TEXT("\\\\.\\COM5"), TEXT("\\\\.\\COM6"), TEXT("\\\\.\\COM7"), TEXT("\\\\.\\COM8"), TEXT("\\\\.\\COM9")};
	int comPort = 0;

	if (!PyArg_ParseTuple(args, "i", &comPort)) {
		return NULL;
	}
	cout << comPort << endl;
	ps.setPort(comList[comPort]);
	if (!ps.open()) {
		return NULL;
	}
	return Py_BuildValue("i", 1);
}

static PyObject *closeSerial(PyObject *self, PyObject *args) {
	if (ps.isOpen()) {
		ps.close();
	}
	return Py_BuildValue("i", 0);
}

static PyMethodDef MethodTable[] = {
	{ "writeSerial", writeSerial, METH_VARARGS, "Send data though serial." },
	{ "readSerial", readSerial, METH_VARARGS, "Read data though serial." },
	{ "openSerial", openSerial, METH_VARARGS, "Open serial." },
	{ "closeSerial", closeSerial, METH_VARARGS, "Close serial." },
	{ 0, 0, 0, 0 }
};

static struct PyModuleDef PicoCrap =
{
	PyModuleDef_HEAD_INIT,
	"PicoCrap",
	"A module for PicoCrap.",
	-1,
	MethodTable
};

PyMODINIT_FUNC PyInit_PicoCrapPYLib() {
	return PyModule_Create(&PicoCrap);
}