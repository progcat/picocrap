#include "PicoCrapPYLib.h"
#include "SerialHandler.h"

using namespace std;

SerialHandler::SerialHandler()
{
	porth = INVALID_HANDLE_VALUE;

	memset(&dcb, 0, sizeof(dcb));

	dcb.DCBlength = sizeof(dcb);
	dcb.BaudRate = CBR_4800;
	dcb.ByteSize = 8;
	dcb.EofChar = 26;
	dcb.EvtChar = 26;
	dcb.XonChar = 17;
	dcb.XoffChar = 19;
	dcb.fDtrControl = DTR_CONTROL_ENABLE;
	dcb.fRtsControl = RTS_CONTROL_DISABLE;
	dcb.XonLim = 2048;
	dcb.XoffLim = 512;
	dcb.Parity = NOPARITY;
	dcb.StopBits = ONESTOPBIT;

	timeout.ReadIntervalTimeout = -1;
	timeout.ReadTotalTimeoutConstant = 0;
	timeout.ReadTotalTimeoutMultiplier = 0;
	timeout.WriteTotalTimeoutConstant = 0;
	timeout.WriteTotalTimeoutMultiplier = 0;
}

SerialHandler::~SerialHandler()
{
	if (porth != INVALID_HANDLE_VALUE) {
		CloseHandle(porth);
	}
}

void SerialHandler::close() {
	if (porth != INVALID_HANDLE_VALUE) {
		CloseHandle(porth);
	}
}

bool SerialHandler::open(TCHAR* device) {
	porth = CreateFile(device, GENERIC_WRITE | GENERIC_READ, 0, NULL, OPEN_EXISTING, NULL, NULL);
	if (porth == INVALID_HANDLE_VALUE) {
		cerr << "Cant open COM port." << endl;
		return false;
	}
	//Set device control block
	if (!SetCommState(porth, &dcb)) {
		cerr << "Cant set DCB." << endl;
		return false;
	}
	//Sets the size of the internal receive buffer 
	if (!SetupComm(porth, 1024, 1024)) {
		cerr << "Cant set buffer size." << endl;
		return false;
	}
	return true;
}

bool SerialHandler::isOpen() {
	return porth != INVALID_HANDLE_VALUE ? true : false;
}

int SerialHandler::read(unsigned char *buff, int length) {
	unsigned long size = 0;
	if (porth != INVALID_HANDLE_VALUE) {
		ReadFile(porth, buff, length, &size, 0);
	}
	return size;
}

int SerialHandler::write(unsigned char *buff, int length) {
	unsigned long size = 0;
	if (porth != INVALID_HANDLE_VALUE) {
		WriteFile(porth, buff, length, &size, 0);
	}
	return size;
}

void SerialHandler::flush() {
	PurgeComm(porth, PURGE_RXCLEAR);
}

bool SerialHandler::setDTR() {
	return EscapeCommFunction(porth, SETDTR);
}

bool SerialHandler::clearDTR() {
	return EscapeCommFunction(porth, CLRDTR);
}

bool SerialHandler::setRTS() {
	return EscapeCommFunction(porth, SETRTS);
}

bool SerialHandler::clearRTS() {
	return EscapeCommFunction(porth, CLRRTS);
}