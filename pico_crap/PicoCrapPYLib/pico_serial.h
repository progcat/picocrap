#pragma once
#include "SerialHandler.h"

#define HARDWARE_DELAY 100

class pico_serial
{
public:
	pico_serial();
	pico_serial(TCHAR *port);
	bool open();
	bool open(TCHAR *port);

	void close();

	bool isOpen();
	bool ping();
	int read(unsigned char *buff, int length);
	int writeCmd(unsigned char *cmd, int length);
	void setPort(TCHAR *port);
private:
	SerialHandler sp;
	TCHAR *Port;
};

