from distutils.core import setup, Extension

module = Extension('PicoCrapPYLib', ['PicoCrapPYLib.cpp', 'pico_serial.cpp', 'SerialHandler.cpp'])
setup(name = 'PicoCrapPYLib', version = '1.0', description = 'This is a module for picocrap', ext_modules = [module])