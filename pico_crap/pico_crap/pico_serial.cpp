#include "stdafx.h"
#include "pico_serial.h"

unsigned char PING[2] = { 0xAC, 0xF8 };
unsigned char PING_RESP[2]= {0x19, 0x1C};

unsigned char CMD_HEADER1[6] = { 0xAC, 0xFE, 0x40, 0x1A, 0x01, 0x00 }; //feedback: 0xEF
unsigned char CMD_HEADER2[6] = { 0xAC, 0xFE, 0x00, 0x1B, 0x01, 0x00 }; //feedback: 0xEF
unsigned char CMD_HEADER3[4] = { 0xAC, 0xFC, 0x08, 0x1A }; //feedback: 0x00 0x00
unsigned char CMD_HEADER4[4] = { 0xAC, 0xFE, 0x40, 0x1A };
unsigned char CMD_END[2] = { 0xAC, 0xFD };


//constructor

pico_serial::pico_serial(TCHAR* port) {
	if (!sp.open(port)) {
		system("pause");
	}
	sp.setDTR();
	sp.clearRTS();
	for (int i = 0; i < 300; i++) {
		sp.write(0x0, 1);
	}
}

//Ping that crap
bool pico_serial::ping() {
	unsigned char input[2];
	sp.clearDTR();
	sp.setDTR();
	for (int i = 0; i < 4; i++) {
		sp.flush();
		sp.write(PING, 2);
		sp.read(&input[0], 1);
		if (input[0] == PING_RESP[0]) {
			sp.read(&input[1], 1);
			if (input[1] == PING_RESP[1] ) {
				return true;
			}
		}
	}
	return false;
}

int pico_serial::read(unsigned char *buff, int length) {
	int size = sp.read(buff, length);
	Sleep(HARDWARE_DELAY);
	return size;
}

int pico_serial::writeCmd(unsigned char *cmd, int length) {
	unsigned char buffer[255];
	int totalLength, dataLength = 0;
	//check pico
	unsigned char input[2];
	if (!ping()) {
		printf("Cant found Pico.\n");
		return 0;
	}
	sp.flush();
	/*HEADER 1*/
	if (!sp.write(CMD_HEADER1, 6)) {
		printf("Cant send header 1.\n");
		return 0;
	}
	Sleep(HARDWARE_DELAY);
	if (!sp.read(input, 1)) {
		printf("Cant get response.\n");
		return 0;
	}
	if (input[0] != 0xEF) {
		printf("Cant verify header 1.\n");
		return 0;
	}
	/*HEADER 2*/
	if (!sp.write(CMD_HEADER2, 6)) {
		printf("Cant send header 2.\n");
		return 0;
	}
	Sleep(HARDWARE_DELAY);
	if (!sp.read(input, 1)) {
		printf("Cant get response.\n");
		return 0;
	}
	if (input[0] != 0xEF) {
		printf("Cant verify header 2.\n");
		return 0;
	}
	/*HEADER 3*/
	if (!sp.write(CMD_HEADER3, 4)) {
		printf("Cant send header 3.\n");
		return 0;
	}
	Sleep(HARDWARE_DELAY);
	if (!sp.read(input, 2)) {
		printf("Cant get response.\n");
		return 0;
	}
	if (input[0] != 0 || input[1] != 0) {
		printf("Cant verify header 3.\n");
		return 0;
	}
	/*Send command*/
	memset(buffer, 0, sizeof(buffer));
	//include the stopbit
	dataLength = length;
	//add header to buffer
	memcpy(&buffer, CMD_HEADER4, sizeof(CMD_HEADER4));
	buffer[4] = dataLength;
	//add our command to buffer
	memcpy(&buffer[5], cmd, length);
	totalLength = dataLength + sizeof(CMD_HEADER4) + 1;

	int size = sp.write(buffer, totalLength);
	if (size == 0) {
		printf("Cant send command.\n");
		return 0;
	}
	Sleep(HARDWARE_DELAY);
	sp.read(input, 1);
	if (input[0] != 0xEF) {
		printf("Dunno is the PICO got all the data.\n");
		return 0;
	}
	sp.write(CMD_END, 2);
	return size;
}

