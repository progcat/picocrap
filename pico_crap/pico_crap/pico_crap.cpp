// pico_crap.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "pico_crap.h"

using namespace std;


int main()
{
	pico_serial sp(TEXT("\\\\.\\COM8"));
	Sleep(100);
	unsigned char output[7] = {0x01, 0x03, 0x01, 0x64, 0x14, 0x13, 0x00};
	unsigned char input[1];
	int size = sp.writeCmd(output, 7);
	printf("%d byte is written.\n", size);

	while (true) {
		sp.read(input, 1);
		printf("Recived: %x\n", (int)input);
		Sleep(HARDWARE_DELAY);
	}
    return 0;
}

