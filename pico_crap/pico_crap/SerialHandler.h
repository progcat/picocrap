#pragma once
#include "stdafx.h"
class SerialHandler
{
public:
	SerialHandler();
	~SerialHandler();

	bool open(TCHAR* device);
	int write(unsigned char *buff, int length);
	int read(unsigned char *buff, int length);
	void flush();

	bool setDTR();
	bool clearDTR();
	bool setRTS();
	bool clearRTS();
private:
	HANDLE porth;
	DCB dcb;
	COMMTIMEOUTS timeout;
};

