#pragma once
#include "SerialHandler.h"

#define HARDWARE_DELAY 100

class pico_serial
{
public:
	pico_serial(TCHAR* port);
	bool ping();
	int read(unsigned char *buff, int length);
	int writeCmd(unsigned char *cmd, int length);
private:
	SerialHandler sp;
};

